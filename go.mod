module gitlab.com/amfjzwsgr29nb0fwchmgtkftqq

go 1.16

require (
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/go-playground/universal-translator v0.18.0 // indirect
	github.com/go-playground/validator v9.31.0+incompatible
	github.com/leodido/go-urn v1.2.1 // indirect
	github.com/stretchr/testify v1.7.0
)
