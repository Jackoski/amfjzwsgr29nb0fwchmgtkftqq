package config

import (
	"github.com/caarlos0/env"
)

type Config struct {
	Port               int    `env:"PORT" envDefault:"8080"`
	APIKey             string `env:"API_KEY" envDefault:"DEMO_KEY"`
	APIURL             string `env:"API_URL,required"`
	ConncurentRequests int    `env:"CONCURRENT_REQUESTS" envDefault:"5"`
	ReadTimeout        int    `env:"READ_TIMEOUT" envDefault:"30"`
	WriteTimeout       int    `env:"WRITE_TIMEOUT" envDefault:"30"`
	IdleTimeout        int    `env:"IDLE_TIMEOUT" envDefault:"120"`
}

func LoadENV() (Config, error) {
	cfg := Config{}
	err := env.Parse(&cfg)
	return cfg, err
}
