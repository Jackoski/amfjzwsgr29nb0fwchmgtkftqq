FROM golang:1.17.1

WORKDIR /src

COPY go.mod ./
COPY go.sum ./
RUN go mod download

COPY . .
WORKDIR /src/cmd/urlcollector
RUN go build -o /urlcollector

EXPOSE 8080

CMD [ "/urlcollector" ]
