package collecturls

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/go-playground/validator"
	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/config"
	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/write"
)

type (
	HttpClient interface {
		Do(req *http.Request) (*http.Response, error)
	}
	apiRes struct {
		URL string `json:"url"`
	}
	res struct {
		URLs []string `json:"urls"`
	}
	chanRes struct {
		URL string
		Err error
	}
	queryParams struct {
		From string `validate:"required"`
		To   string `validate:"required"`
	}
	validatedParams struct {
		from time.Time
		to   time.Time
	}
	Handler struct {
		apiKey string
		apiUrl string
		ccReq  int
		log    *log.Logger
		write  write.Write
		client HttpClient
	}
)

const DAY = 1

func New(l *log.Logger, w write.Write, cfg config.Config, ct HttpClient) Handler {
	return Handler{
		apiKey: cfg.APIKey,
		apiUrl: cfg.APIURL,
		ccReq:  cfg.ConncurentRequests,
		log:    l,
		write:  w,
		client: ct,
	}
}

func (h *Handler) Controller(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		h.collectUrls(w, r)
	default:
		h.write.NotFound(w, r)
	}
}

func (h *Handler) collectUrls(w http.ResponseWriter, r *http.Request) {
	params, err := h.validateQuery(r.URL.Query())
	if err != nil {
		h.log.Printf("collectUrls.validateQuery, err: %v", err)
		h.write.CustomBadRequest(w, err.Error())
		return
	}
	resCh := make(chan chanRes, h.ccReq)
	var wg sync.WaitGroup
	for !params.from.After(params.to) {
		wg.Add(1)
		go h.getUrls(resCh, params, &wg)
		params.from = params.from.AddDate(0, 0, DAY)
	}
	go func() {
		defer close(resCh)
		wg.Wait()
	}()
	res := res{}
	for r := range resCh {
		if r.Err != nil {
			h.log.Printf("collectUrls.go: %v", err)
			h.write.CustomBadRequest(w, "cannot get urls")
			return
		}
		res.URLs = append(res.URLs, r.URL)
	}
	h.write.JSON(w, res, "collectUrls.res")
	return
}

func (h *Handler) validateQuery(params url.Values) (validatedParams, error) {
	validate := validator.New()
	fromQuery := params.Get("from")
	toQuery := params.Get("to")
	if err := validate.Struct(queryParams{From: fromQuery, To: toQuery}); err != nil {
		return validatedParams{}, err
	}
	from, err := time.Parse("2006-01-02", fromQuery)
	if err != nil {
		return validatedParams{}, fmt.Errorf("Cannot parse date from, err: %v, val: %s", err, from)
	}
	to, err := time.Parse("2006-01-02", toQuery)
	if err != nil {
		return validatedParams{}, fmt.Errorf("Cannot parse date to, err: %v, val: %s", err, to)
	}
	if from.After(to) {
		return validatedParams{}, fmt.Errorf(`invalid date range provided: from '%s' to '%s'`, from, to)
	}
	queryParams := validatedParams{from: from, to: to}
	return queryParams, nil
}

func (h *Handler) getUrls(ch chan chanRes, params validatedParams, wg *sync.WaitGroup) {
	defer wg.Done()
	req, err := h.buildRequest(params)
	if err != nil {
		ch <- chanRes{Err: err}
		return
	}
	r, err := h.client.Do(req)
	if err != nil {
		ch <- chanRes{Err: err}
		return
	}
	if r.StatusCode != http.StatusOK {
		ch <- chanRes{Err: fmt.Errorf(r.Status)}
		return
	}
	defer h.closeBody(r.Body)
	res := apiRes{}
	if err = json.NewDecoder(r.Body).Decode(&res); err != nil {
		ch <- chanRes{Err: err}
		return
	}
	ch <- chanRes{URL: res.URL}
}

func (h *Handler) buildRequest(rp validatedParams) (*http.Request, error) {
	u, err := url.Parse(h.apiUrl)
	if err != nil {
		return nil, err
	}
	q := u.Query()
	q.Set("api_key", h.apiKey)
	q.Set("date", rp.from.Format("2006-01-02"))
	u.RawQuery = q.Encode()
	return http.NewRequest(http.MethodGet, u.String(), nil)
}

func (h *Handler) closeBody(c io.ReadCloser) {
	if err := c.Close(); err != nil {
		h.log.Printf("getUrls.close.body: %v", err)
	}
}
