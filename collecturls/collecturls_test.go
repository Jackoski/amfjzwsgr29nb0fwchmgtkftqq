package collecturls

import (
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/config"
	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/write"
)

func Test_CollectUrls_OK_200(t *testing.T) {
	httpClient := HttpClientMock{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			return &http.Response{
				StatusCode: http.StatusOK,
				Body:       io.NopCloser(strings.NewReader(`{"url":"https://www.some.url/nice/picture.jpg"}`)),
			}, nil
		},
	}
	apiUrl := "https://www.some.url"
	srv := setupServer(t, &httpClient, apiUrl)
	defer srv.Close()
	t.Run("When no error", func(t *testing.T) {
		url := fmt.Sprintf("%s/pictures?from=2020-10-10&to=2020-10-10", srv.URL)
		request, err := http.NewRequest(http.MethodGet, url, nil)
		require.NoError(t, err)
		resp, err := srv.Client().Do(request)
		require.NoError(t, err)
		defer func() {
			require.NoError(t, resp.Body.Close())
		}()
		assert.Equal(t, http.StatusOK, resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		require.NoError(t, err)
		expected := fmt.Sprintln(`{"urls":["https://www.some.url/nice/picture.jpg"]}`)
		assert.Equal(t, expected, string(body))
	})
}

func Test_CollectUrls_BadRequest_400(t *testing.T) {
	testCases := []struct {
		name         string
		requestedUrl string
		expected     string
		status       string
		respStatus   int
		apiUrl       string
		respJSON     string
		statusCode   int
		err          error
	}{
		{
			name:         "when 'to' param is invalid",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			requestedUrl: `pictures?from=2020-10-10&to=2020-10-`,
			expected:     `{"error":"Cannot parse date to, err: parsing time "2020-10-" as "2006-01-02": cannot parse "" as "02", val: 0001-01-01 00:00:00 +0000 UTC"}`,
		},
		{
			name:         "when 'to' param is missing",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			requestedUrl: `pictures?from=2020-10-10`,
			expected:     `{"error":"Key: 'queryParams.To' Error:Field validation for 'To' failed on the 'required' tag"}`,
		},
		{
			name:         "when 'from' param is missing",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			requestedUrl: `pictures?to=2020-10-10`,
			expected:     `{"error":"Key: 'queryParams.From' Error:Field validation for 'From' failed on the 'required' tag"}`,
		},
		{
			name:         "when 'from' param is invalid",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			requestedUrl: `pictures?from=2020-10&to=2020-10-10`,
			expected:     `{"error":"Cannot parse date from, err: parsing time "2020-10" as "2006-01-02": cannot parse "" as "-", val: 0001-01-01 00:00:00 +0000 UTC"}`,
		},
		{
			name:         "when 'from' is after 'to'",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			requestedUrl: `pictures?from=2020-10-11&to=2020-10-10`,
			expected:     `{"error":"invalid date range provided: from '2020-10-11 00:00:00 +0000 UTC' to '2020-10-10 00:00:00 +0000 UTC'"}`,
		},
		{
			name:         "api request returns not OK http status",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			respStatus:   http.StatusBadRequest,
			requestedUrl: `pictures?from=2020-10-10&to=2020-10-10`,
			expected:     `{"error":"cannot get urls"}`,
		},
		{
			name:         "api request fails",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			respStatus:   http.StatusOK,
			err:          fmt.Errorf("api error"),
			requestedUrl: `pictures?from=2020-10-10&to=2020-10-10`,
			expected:     `{"error":"cannot get urls"}`,
		},
		{
			name:         "api returns invalid json",
			apiUrl:       "https://www.some.url",
			statusCode:   http.StatusBadRequest,
			respStatus:   http.StatusOK,
			respJSON:     `{"invalid:"json"`,
			requestedUrl: `pictures?from=2020-10-10&to=2020-10-10`,
			expected:     `{"error":"cannot get urls"}`,
		},
		{
			name:         "api url in incorrect",
			apiUrl:       "hs://w-34%^ww.some.url",
			statusCode:   http.StatusBadRequest,
			respStatus:   http.StatusOK,
			respJSON:     `{"invalid:"json"`,
			requestedUrl: `pictures?from=2020-10-10&to=2020-10-10`,
			expected:     `{"error":"cannot get urls"}`,
		},
	}
	for _, tc := range testCases {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			httpClient := HttpClientMock{
				DoFunc: func(req *http.Request) (*http.Response, error) {
					return &http.Response{
						Status:     tc.status,
						StatusCode: tc.respStatus,
						Body:       io.NopCloser(strings.NewReader(tc.respJSON)),
					}, tc.err
				},
			}
			srv := setupServer(t, &httpClient, tc.apiUrl)
			defer srv.Close()
			url := fmt.Sprintf("%s/%s", srv.URL, tc.requestedUrl)
			request, err := http.NewRequest(http.MethodGet, url, nil)
			require.NoError(t, err)
			resp, err := srv.Client().Do(request)
			require.NoError(t, err)
			defer func() {
				require.NoError(t, resp.Body.Close())
			}()
			assert.Equal(t, tc.statusCode, resp.StatusCode)
			body, err := ioutil.ReadAll(resp.Body)
			require.NoError(t, err)
			assert.Equal(t, tc.expected, string(body))
		})
	}
}

func Test_CollectUrls_NotFound_404(t *testing.T) {
	httpClient := HttpClientMock{
		DoFunc: func(req *http.Request) (*http.Response, error) {
			return nil, nil
		},
	}
	apiUrl := "https://www.some.url"
	srv := setupServer(t, &httpClient, apiUrl)
	defer srv.Close()
	t.Run("When srv url not found", func(t *testing.T) {
		url := fmt.Sprintf("%s/not/existing?from=2020-10-10&to=2020-10-10", srv.URL)
		request, err := http.NewRequest(http.MethodGet, url, nil)
		require.NoError(t, err)
		resp, err := srv.Client().Do(request)
		require.NoError(t, err)
		defer func() {
			require.NoError(t, resp.Body.Close())
		}()
		assert.Equal(t, http.StatusNotFound, resp.StatusCode)
		body, err := ioutil.ReadAll(resp.Body)
		require.NoError(t, err)
		expected := `{"error":"Not found"}`
		assert.Equal(t, expected, string(body))
	})
}

func setupServer(t *testing.T, httpClient HttpClient, apiUrl string) *httptest.Server {
	logger := log.New(io.Discard, "test", log.Default().Flags())
	cfg := config.Config{
		APIURL: apiUrl,
	}
	wr := write.New(logger)
	ch := New(logger, wr, cfg, httpClient)
	router := http.NewServeMux()
	router.HandleFunc("/pictures", ch.Controller)
	router.HandleFunc("/", wr.NotFound)

	server := httptest.NewServer(router)
	return server
}
