# amFjZWsgR29nb0FwcHMgTkFTQQ

Service collecting image and video urls

## Purpose of the project

This is a REST API web application.

The API allows to get NASA urls list:

Publicly exposed application

## Project setup

```
git clone git@gitlab.com:Jackoski/amfjzwsgr29nb0fwchmgtkftqq.git
cd amfjzwsgr29nb0fwchmgtkftqq
```

```
docker build --tag urlcollector .
```

```
cp .env.example .env
```

```
...
docker run --env-file .env -p 8080:8080 urlcollector
```

Web API is running at [http://localhost:8080/](http://localhost:8080/)

### env variables

All env variables used by application are declared in `.env.example` file

| Name                      | Type      | Required  | Description                                                     |
| :------------------------ | :-------- | :-------- | :-------------------------------------------------------------- |
| `PORT`                    | `integer` | **true**  | Port on which the app will be listening on. Example: `8080`     |
| `API_KEY`                 | `string`  | **false** | api key that allow to fetch urls Example DEMO_KEY               |
| `API_URL`                 | `string`  | **true**  | Url to external api provider. Exmaple: "https://www.google.com" |
| `CONCURRENT_REQUESTS`     | `int`     | **false** | Max goroutines that can fetch external api. Example: 5          |
| `READ_TIMEOUT`            | `int`     | **false** | HTTP read timeout in seconds. Example: 30                       |
| `WRITE_TIMEOUT`           | `int`     | **false** | HTTP write timeout in seconds. Example: 30                      |
| `IDLE_TIMEOUT`            | `int`     | **false** | HTTP idle timeout in seconds. Example: 120                      |
