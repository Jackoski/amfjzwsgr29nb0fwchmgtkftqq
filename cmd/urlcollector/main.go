package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/collecturls"
	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/config"
	"gitlab.com/amfjzwsgr29nb0fwchmgtkftqq/write"
)

var appName = "URL collector "

func main() {
	l := log.New(os.Stdout, appName, log.Lshortfile|log.LstdFlags)
	cfg, err := config.LoadENV()
	if err != nil {
		l.Fatalf("Cannot load env vars: %v", err)
		return
	}
	setupRoutes(l, cfg)
	server := http.Server{
		Addr:         fmt.Sprintf(":%d", cfg.Port),
		Handler:      nil,
		ReadTimeout:  time.Duration(cfg.ReadTimeout) * time.Second,
		WriteTimeout: time.Duration(cfg.WriteTimeout) * time.Second,
		IdleTimeout:  time.Duration(cfg.IdleTimeout) * time.Second,
	}

	go startAppServer(&server, appName, l)

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	sig := <-c

	l.Printf(fmt.Sprintf("Got Signal: %s", sig))
}

func startAppServer(server *http.Server, appName string, l *log.Logger) {
	l.Printf(fmt.Sprintf("Starting %son %s", appName, server.Addr))
	err := server.ListenAndServe()
	if errors.Is(err, http.ErrServerClosed) {
		l.Printf(fmt.Sprintf("Stopped correctly. Reason: %v", err))
	} else {
		l.Fatalf(fmt.Sprintf("Error starting application: %v", err))
	}
}

func setupRoutes(l *log.Logger, cfg config.Config) {
	ct := http.Client{Timeout: time.Duration(cfg.ReadTimeout) * time.Second}
	wr := write.New(l)
	ch := collecturls.New(l, wr, cfg, &ct)
	http.HandleFunc("/pictures", ch.Controller)
	http.HandleFunc("/", wr.NotFound)
}
