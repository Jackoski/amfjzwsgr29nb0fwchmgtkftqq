package write

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type (
	Write struct {
		l *log.Logger
	}
)

func New(l *log.Logger) Write {
	return Write{
		l: l,
	}
}

func (wr *Write) NotFound(w http.ResponseWriter, r *http.Request) {
	wr.Error(w, http.StatusNotFound, "Not found")
}

func (wr *Write) CustomBadRequest(w http.ResponseWriter, msg string) {
	wr.Error(w, http.StatusBadRequest, msg)
}

func (wr *Write) Error(w http.ResponseWriter, code int, msg string) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(code)
	i, err := w.Write([]byte(fmt.Sprintf(`{"error":"%s"}`, msg)))
	if err != nil {
		wr.l.Printf("Cannot send error res, error: %v, written: %d", err, i)
	}
	return
}

func (wr *Write) JSON(w http.ResponseWriter, body interface{}, prefix string) {
	w.Header().Add("Content-Type", "application/json")
	err := json.NewEncoder(w).Encode(body)
	if err != nil {
		wr.l.Printf("%s cannot send response: %+v", prefix, body)
		wr.Error(w, http.StatusInternalServerError, "Cannot send response")
		return
	}
}
